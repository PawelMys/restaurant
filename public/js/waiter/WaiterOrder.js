class WaiterOrder {
    constructor() {
        this.selectors = {
            orderId: $("#order"),
            orderBtn: $(".order-btn")
        };

        this.bindEvents();
    }

    bindEvents()
    {
        this.selectors.orderBtn.click(this.orderItem.bind(this))
    }

    orderItem(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        const orderId = this.selectors.orderId.attr("order-id");
        let dishId = element.attr("item-id");
        let quantity = element.parent().prev().children("input.count").val();
        let data = {
            orderId: parseInt(orderId),
            id: parseInt(dishId),
            quantity: parseInt(quantity),
            isConfirm: 1
        };
        fetch("/waiter/orders/add-dish", {
            method: 'POST',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(data)
        }).then((res) => res.json())
            .then(res => {
                if (res.type === "success") {
                    location.reload();
                }
            })
            .catch();
    }
}

$(document).ready(function(){
    new WaiterOrder();
});