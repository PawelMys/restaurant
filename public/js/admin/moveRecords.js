class MoveRecords {

    constructor() {
        this.container = $('.container-fluid');

        this.buttons = {
            eventRecords: this.container.find(".up,.down"),
        };

        this.bindEvents();
    }

    bindEvents() {
        this.buttons.eventRecords.click(this.moveRecords.bind(this));
    }

    moveRecords(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        let row = this.actualRow(element);
        let send = {};
        const url = this.getUrl(element);
        send.position = this.actualPosition(row);
        send.categoryId = this.actualCategoryId(row);
        if (this.isRowMoveUp(element, row)) {
            send.move = 'up';
            this.sendJson(send, row, url)
        } else if (this.isRowMoveDown(element, row)) {
            send.move = 'down';
            this.sendJson(send, row, url)
        }
    }

    actualRow(element) {
        return element.parents("tr:first");
    }

    actualPosition(row) {
        return row.children('.dish-position').text();
    }

    actualCategoryId(row) {
        return row.attr('category');
    }

    getUrl(element) {
        return element.attr('href');
    }

    isRowMoveUp(element, row) {
        if (element.is(".up") && row.prev().length !== 0) {
            return true;
        }
    }

    isRowMoveDown(element, row) {
        if (element.is(".down") && row.next().length !== 0) {
        return true;
        }
    }

    sendJson(send, row, url) {
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(send)
        })
            .then((res) => res.json())
            .then(res => {
                this.successSend(row, res)
            })
    }

    successSend(row, res) {
        if (res.move === 'up') {
            this.insertBefore(row, res);
        } else if (res.move === 'down') {
            this.insertAfter(row, res);
        }
    }

    insertBefore(row, res) {
        row.children('.dish-position').html(res.newPositionNumber);
        row.insertBefore(row.prev());
        row.next().children('.dish-position').html(res.oldPositionNumber);
    }

    insertAfter(row, res) {
        row.children('.dish-position').html(res.newPositionNumber);
        row.insertAfter(row.next());
        row.prev().children('.dish-position').html(res.oldPositionNumber);
    }
}

$(document).ready(function(){
    new MoveRecords();
});