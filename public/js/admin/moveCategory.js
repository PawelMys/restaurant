class MoveCategory {
    constructor()
    {
        this.container = $('.container-fluid');

        this.buttons = {
            eventCategory: this.container.find(".categoryUp,.categoryDown"),
        };

        this.bindEvents();

        // this.errorHandler = CategoryErrorHandler(this);
        //
        // this.errorHandler.handle(errors);

    }

    bindEvents() {
        this.buttons.eventCategory.click(this.moveCategory.bind(this));
    }

    moveCategory(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        let positionNumber = this.actualPositionNumber(element);
        const div = this.actualPosition(element, positionNumber);
        const url = this.actualUrl(element);
        let send = {};
        send.position = positionNumber;
        if (this.trueIfCategoryUp(element, div)) {
            send.move = 'up';
            this.sendJson(send, div, url);
        } else if (this.trueIfCategoryDown(element, div)) {
            send.move = 'down';
            this.sendJson(send, div, url);
        }
    }

    trueIfCategoryUp(element, div) {
        if (element.is(".categoryUp") && div.prev().children().is('.move')) {
            return true;
        }
    }

    trueIfCategoryDown(element, div) {
        if (element.is(".categoryDown") && div.next().children().is('.move')) {
            return true;
        }
    }

    actualPositionNumber(element) {
        return element.parents("div:first").attr('position');
    }

    actualUrl(element) {
        return element.attr('href');
    }

    actualPosition(element, positionNumber) {
        return element.parents("div:first").parents("#category" + positionNumber);
    }

    sendJson(send, div, url) {
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(send)
        })
            .then((res) => res.json())
            .then(res => {
                this.successSend(res, div)
        })
            .catch()
    }

    successSend(res, div) {
        if (res.move === 'up') {
            this.insertBefore(div, res);
        } else if (res.move === 'down') {
            this.insertAfter(div, res);
        }
    }

    insertBefore(div, res) {
        div.insertBefore(res.newPositionDiv);
        div.attr('id', 'category' + res.newPositionNumber);
        div.children('.move').attr('position', res.newPositionNumber);
        div.next().attr('id', 'category' + res.oldPositionNumber);
        div.next().children('.move').attr('position', res.oldPositionNumber);
    }

    insertAfter(div, res) {
        div.insertAfter(res.newPositionDiv);
        div.attr('id', 'category' + res.newPositionNumber);
        div.children('.move').attr('position', res.newPositionNumber);
        div.prev().attr('id', 'category' + res.oldPositionNumber);
        div.prev().children('.move').attr('position', res.oldPositionNumber);
    }
}

$(document).ready(function(){
    new MoveCategory();
});


// class CategoryErrorHandler {
//     constructor(category) {
//         this.category = category;
//     }
//
//     handle() {
//         this.category.buttons.categoryUp.addClass('has-error');
//     }
// }