
class ClientOrder extends ClientSendOrder{
    constructor() {
        super();            quantity: $('')

        this.price = $('#total-price');

        this.buttons = {
            order: $('.order-dish'),
            qtyPlus: $('.plus'),
            qtyMinus: $('.minus')
        };

        this.bindEvents();
    }

    bindEvents() {
        this.buttons.order.click(this.order.bind(this));
        this.buttons.qtyPlus.click(this.plus.bind(this));
        this.buttons.qtyMinus.click(this.minus.bind(this));
    }

    plus(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        let data = this.countData(element, 'plus');
        this.sendQtyJson(data, 'plus');

        console.log(data['input']);
    }
    minus(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        let data = this.countData(element, 'minus');
        this.sendQtyJson(data, 'minus');
    }

    countData(element, type) {
        let data = [];
        if (type === 'plus') {
            data['input'] = element.prev();
        } else {
            data['input'] = element.next();
        }
        data['value'] = data['input'].val();
        data['target'] = data['input'].attr('item-id');

        return data
    }

    sendQtyJson(value, type) {
        let sendItem = {
            'target': parseInt(value['target']),
            'value': value['value'],
            'type': type
        };
        fetch("/client/order/qty", {
            method: 'POST',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(sendItem)
        }).then((res) => res.json())
            .then(res => {
                value['input'].val(res.quantity);
                this.price.text(res.totalPrice.toFixed(2));
            })
            .catch();
    }

    order(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        let url = this.getUrl(element);
        let grabFirstDiv = this.grabFirstDiv(element);
        let order = {
            price: this.orderPrice(grabFirstDiv),
            name: this.orderName(grabFirstDiv),
            id: this.orderId(grabFirstDiv),
            isConfirm: 0,
            quantity: 1
        };

        fetch(url, {
            method: 'POST',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(order)
        }).then((res) => res.json())
            .then(res => {
                $("#ordered").append(this.appendDiv(order.name, res.id, order.price));
                this.price.text(res.totalPrice.toFixed(2));
                location.reload();
            })
            .catch();
    }

    grabFirstDiv(element) {
        return element.parents("div:first");
    }

    orderPrice(grabFirstDiv) {
        return grabFirstDiv.children("span").text();
    }

    orderName(grabFirstDiv) {
        return grabFirstDiv.parents("div:first").children("div:first").children("h5").text();
    }

    orderId(grabFirstDiv) {
        return grabFirstDiv.parents("div:first").children("div:first").attr('item-id');
    }

    getUrl(element)
    {
        return element.attr("href");
    }

    appendDiv(name, id, price) {
        return"<div class=\"row-item row border-bottom mb-4 pb-2\">\n" +
            "<div class=\"col-md-6\">" + name + "</div>\n" +
            "    <div class=\"col-md-3 qty\">\n" +
            "        <span class=\"minus bg-danger\">-</span>\n" +
            "        <input type=\"number\" class=\"count\" name=\"qty\" value=\"1\" item-id=\""+  id + "\">\n" +
            "        <span class=\"plus bg-success\">+</span>\n" +
            "    </div>" +
            "<div class=\"col-md-1 text-center\">" + price + "</div>\n" +
            "<div class=\"col-md-2\"><i class=\"fas fa-trash-alt delete-item\" item-delete-id=\"" + id + "\"></i></div>\n" +
            "</div>";
    }

}

$(document).ready(function(){
    new ClientOrder();
});
