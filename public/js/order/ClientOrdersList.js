class ClientOrdersList {
    constructor() {

        this.seconds = 30;

        this.selectors = {
            remove: $(".item-delete"),
            options: $(".options"),
            price: $(".total-price")
        };

        setInterval(this.refresh, this.seconds*1000);
        this.bindEvents();
    }

    refresh() {
        window.location.reload(1);
    }

    bindEvents() {
        this.selectors.remove.on("click", this.removeDish.bind(this))
    }

    removeDish(e) {
        e.preventDefault();
        let element = $(e.currentTarget);
        let url = element.attr("href");
        let id = element.attr("item-delete");
        let itemRemove = element.parents().eq(1);
        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(id)
        }).then((res) => res.json())
            .then(res => {
                if (res.removedId === parseInt(id)) {
                    itemRemove.remove();
                }
                this.selectors.price.text(res.totalPrice.toFixed(2));
            })
            .catch();
    }

}

$(document).ready(function(){
    new ClientOrdersList();
});