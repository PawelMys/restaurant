class RemoveOrder {
    constructor() {
        this.remove = {
            button: ".row-item i",
            removeIn:  $(".modal-body")
        };

        this.price = $('#total-price');

        this.bindEvents();

    }

    bindEvents() {
        this.remove.removeIn.on('click', this.remove.button, this.removeRow.bind(this));
    }

    removeRow(e) {
        let element = $(e.currentTarget);
        let url = "/client/order/remove";
        let itemRemove = element.parents().eq(1);
        let id = element.attr("item-delete-id");
        fetch(url, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(id)
        }).then((res) => res.json())
            .then(res => {
                if (res.removedId === parseInt(id)) {
                    itemRemove.remove();
                }
                this.price.text(res.totalPrice.toFixed(2));
            })
            .catch();
    }
}

$(document).ready(function(){
    new RemoveOrder();
});