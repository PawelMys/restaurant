class ClientSendOrder{

    sendJson(url, json)
    {
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(json)
        })
    }
}