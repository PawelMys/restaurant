class KitchenOrder {
    constructor() {

        this.seconds = 30;

        this.selectors = {
            order: ".orders-active a",
            errorMsg: $("#jsMessages"),
            refreshBtn: $(".refresh-btn")
        };

        this.bindEvents();

        this.rel = setInterval(this.reload, this.seconds*1000);
    }

    reload() {
        $("#all-orders").load(window.location.href +  ' #kitchen-orders');
        console.log('reload');
    }

    bindEvents() {
        $("#all-orders").on('click', this.selectors.order,  this.takeOrder.bind(this));
        this.selectors.refreshBtn.on('click', this.refreshPage.bind(this));
    }

    takeOrder(e) {
        e.preventDefault();
        const element = $(e.currentTarget);
        console.log(element);
        let menuId = this.getMenuId(element);
        let orderId = this.getOrderId(element);
        let sendData = {
            orderId: orderId,
            menuId: menuId
        };
        let url = this.getUrl(element);
        fetch(url , {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            body: JSON.stringify(sendData)
        }).then((res) => res.json())
            .then(res => {
                if (res.errorMsg) {
                    this.selectors.errorMsg.empty().append("<div class=\"alert alert-danger\">\n" +
                        res.errorMsg +
                        "</div>");
                    console.log(res.errorMsg)
                } else {
                    this.selectors.errorMsg.empty();
                    this.markItem(element);
                    this.createFinishButton(element, menuId, orderId);
                }
            })

    }

    getMenuId(element) {
        return element.attr("item-id");
    }

    getOrderId(element) {
        return element.parents("div .card-body").prev("div").attr("order-id");
    }

    getUrl(element) {
        return element.attr("href");
    }

    markItem(element) {
        return element.addClass("kitchen-active-order");
    }

    createFinishButton(element, menuId, orderId) {
        const selector = $(".check" + orderId + menuId);
        selector.removeClass("btn disabled text-black-50");
        selector.children("i:first").addClass("text-success");

    }

    refreshPage() {
        window.location.reload(1);
    }

}

$(document).ready(function(){
    new KitchenOrder();
});