<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 13.12.18
 * Time: 16:21
 */

namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [
            new TwigFunction('printMenu', [
                $this, 'adminMenu'],
                [
                    'needs_environment' => true,
                    'is_safe' => ['html']
                ])
        ];
    }

    public function adminMenu(\Twig_Environment $environment, array $menuArr)
    {

        $menu = [];

        foreach ($menuArr as $data) {
            $item = [$data[0] => $data[1]];
            array_push($menu, $item);
        }
        return $environment->render('modules/menu.html.twig', [
            'menu' => $menu
        ]);
    }
}