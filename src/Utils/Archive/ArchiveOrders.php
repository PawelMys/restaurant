<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.12.18
 * Time: 14:32
 */

namespace App\Utils\Archive;


use App\Entity\ArchiveOrder;
use App\Utils\AbstractDoctrineUtils;

class ArchiveOrders extends AbstractDoctrineUtils
{
    public function archiveOrder($clientOrder, $user)
    {
        $archive = new ArchiveOrder();
        $this->moveToArchive($clientOrder, $archive, $user);
    }

    public function moveToArchive($order, $archive, $user)
    {
        $orderDetails = $this->moveDataToArray($order);
        $archive->setPrice($order->getPrice());
        $archive->setOrderedTime($order->getDate());
        $archive->setOrderId($order->getId());
        $archive->setWaiter($user);
        $archive->setPaidTime(new \DateTime('now'));
        $archive->setOrderedDish($orderDetails);
        $this->loadToBase($order, $archive);
    }

    public function moveDataToArray($order)
    {
        $dataArray = [];
        foreach($order->getOrdered() as $data) {
            $actual = [
                'menu' => $data->getMenu()->getCategory()->getName(),
                'dish' => $data->getMenu()->getDish()->getName(),
                'cook' => $data->getKitchen()->getCook()->getUsername()
            ];
            array_push($dataArray, $actual);
        }
        return $dataArray;
    }

    public function loadToBase($order, $archive)
    {
        $em = $this->doctrine;
        $em->persist($archive);
        $em->remove($order);
        $em->flush();
    }
}