<?php

namespace App\Utils\Menu;

use App\Entity\Menu;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class MenuPosition extends AbstractDoctrineUtils
{

    public function findData($move) {
        $repo = $this->doctrine->getRepository(Menu::class);
        $movedDish = $repo->findOneBy([
            'category' => $move->categoryId,
            'dishPosition' => (float)$move->position
        ]);

        return $movedDish;
    }

    public function moveControl($move, $movedDish)
    {
        if ($move->move == 'up') {

            return $this->moveMenu($move, $movedDish, -1);

        } elseif ($move->move == 'down') {

            return $this->moveMenu($move, $movedDish, 1);
        }
    }

    private function moveMenu($move, $movedDish, $changePosition) {
        $notMovedDish = $this->findNotMovedMenu($move, $changePosition);
        if ($notMovedDish == null) {
            return new Response('Error!', 400);
        } else {
            $newNotMovedDishPosition = $this->newPositionOfNotMovedDish($notMovedDish, $changePosition);
            $notMovedDish->setDishPosition($newNotMovedDishPosition);
            $newMovedDishPosition = $this->newPositionOfMovedDish($movedDish, $changePosition);
            $movedDish->setDishPosition($newMovedDishPosition);
            $this->savePositions($movedDish, $notMovedDish);

            return new JsonResponse([
                'move' => $move->move,
                'newPositionNumber' => $newMovedDishPosition,
                'oldPositionNumber' => $move->position
            ]);
        }
    }

    private function findNotMovedMenu($move, $changePosition)
    {
        $repo = $this->doctrine->getRepository(Menu::class);

        return $repo->findOneBy([
            'category' => $move->categoryId,
            'dishPosition' => (float)$move->position + $changePosition
        ]);
    }

    private function newPositionOfNotMovedDish($dish, $changePosition)
    {
        $oldPosition = $dish->getDishPosition();

        return $oldPosition - $changePosition;
    }

    private function newPositionOfMovedDish($movedDish, $changePosition)
    {
        $oldMovedDishPosition = $movedDish->getDishPosition();

        return $oldMovedDishPosition + $changePosition;
    }

    private function savePositions($movedDish, $notMovedDish)
    {
        $em = $this->doctrine;
        try {
            $em->persist($movedDish);
        } catch (ORMException $e) {
        }
        try {
            $em->persist($notMovedDish);
        } catch (ORMException $e) {
        }
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }
    }
}