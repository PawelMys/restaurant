<?php

namespace App\Utils\Menu;

use App\Entity\Category;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CategoryPosition extends AbstractDoctrineUtils
{
    public function findData($move) {
        $repo = $this->doctrine->getRepository(Category::class);
        $moveCategory = $repo->findOneBy([
            'position' => $move->position,
        ]);

        return $moveCategory;
    }

    public function moveControl($move, $movedCategory)
    {
        if ($move->move == 'up') {

            return $this->moveCategory($move, $movedCategory, -1);

        } elseif ($move->move == 'down') {

            return $this->moveCategory($move, $movedCategory, 1);
        }
    }

    public function moveCategory($move, $movedCategory, $changePosition)
    {
        $notMovedCategory = $this->findNotMovedCategory($move, $changePosition);
        if ($notMovedCategory == null) {

            return new Response('Error!', 400);
        } else {
            $newNotMovedCategoryPosition = $this->newNotMovedCategoryPosition($notMovedCategory, $changePosition);
            $notMovedCategory->setPosition($newNotMovedCategoryPosition);
            $newMovedCategoryPosition = $this->newMovedCategoryPosition($movedCategory, $changePosition);
            $movedCategory->setPosition($newMovedCategoryPosition);
            $this->savePosition($movedCategory, $notMovedCategory);

            return new JsonResponse([
                'move' => $move->move,
                'newPositionNumber' => $newMovedCategoryPosition,
                'newPositionDiv' => '#category'. $newMovedCategoryPosition,
                'oldPositionNumber' => $move->position
            ]);
        }
    }

    public function findNotMovedCategory($move, $changePosition)
    {
        $repo = $this->doctrine->getRepository(Category::class);

         return $repo->findOneBy([
            'position' => (float)$move->position + $changePosition,
        ]);
    }

    public function newNotMovedCategoryPosition($notMovedCategory, $changePosition)
    {
        $oldPosition = $notMovedCategory->getPosition();

        return $oldPosition - $changePosition;
    }

    public function newMovedCategoryPosition($movedCategory, $changePosition)
    {
        $oldMovedCategoryPosition = $movedCategory->getPosition();

        return $oldMovedCategoryPosition + $changePosition;
    }

    public function savePosition($movedCategory, $notMovedCategory)
    {
        $em = $this->doctrine;
        try {
            $em->persist($movedCategory);
        } catch (ORMException $e) {
        }
        try {
            $em->persist($notMovedCategory);
        } catch (ORMException $e) {
        }
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }
    }
}