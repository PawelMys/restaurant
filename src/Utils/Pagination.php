<?php

namespace App\Utils;


use App\Entity\Menu;
use Doctrine\ORM\EntityManager;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class Pagination
{

    /**
     * @var EntityManager
     */
    private $doctrine;

    /**
     * Pagination constructor.
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;

    }


    public function paginateAll($class, $page)
    {
        $repo = $this->doctrine->getRepository($class);
        $data = $repo->findAllQueryBuilder();
        $adapter = new DoctrineORMAdapter($data);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($page);
        return $pagerfanta;
    }

    public function paginateSortMenu($page)
    {
        $repo = $this->doctrine->getRepository(Menu::class);
        $data = $repo->sortMenu();
        $adapter = new DoctrineORMAdapter($data);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($page);
        return $pagerfanta;
    }
}