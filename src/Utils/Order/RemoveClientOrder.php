<?php

namespace App\Utils\Order;

use App\Entity\ClientOrder;
use App\Entity\Dish;
use App\Entity\Menu;
use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;

class RemoveClientOrder extends AbstractDoctrineUtils
{

    private $totalPrice;

    public function __construct(EntityManager $doctrine, Price $totalPrice)
    {
        parent::__construct($doctrine);
        $this->totalPrice = $totalPrice;
    }

    public function removeItem($menuOrderId, $user)
    {
        $em = $this->doctrine;
        $clientOrder = $this->findClientOrder($user);
        $menuOrder = $this->findMenuOrder($menuOrderId, $clientOrder);
        $removedId = $menuOrder->getId();
        $newPrice = $this->totalPrice->removeDish($clientOrder, $menuOrder);
        $clientOrder->setPrice($newPrice);
        $em->persist($clientOrder);
        $em->remove($menuOrder);
        $em->flush();
        return new JsonResponse([
            'removedId' => $removedId,
            'totalPrice' => $newPrice
            ]);
    }

    public function findClientOrder($user)
    {
        $repo = $this->doctrine->getRepository(ClientOrder::class);
        $order = $repo->findOneBy([
            'client' => $user
        ]);
        return $order;
    }

    public function findMenuOrder($id, $clientOrder)
    {
        $repo = $this->doctrine->getRepository(OrderedMenu::class);
        $order = $repo->findOneBy([
            'id' => $id,
            'ordered' => $clientOrder,
            'isConfirm' => 0
        ]);
        return $this->isOrderExist($order);
    }

//    public function changePrice($waiterOrder, $menuOrder, $quantity)
//    {
//        $orderedMenuId = $menuOrder->getMenu()->getId();
//        $oldPrice = $waiterOrder->getPrice();
//        $dishPrice = $this->getDishPrice($orderedMenuId);
//        return $newPrice = $oldPrice - $quantity*$dishPrice;
//    }

    public function getDishPrice($menuId)
    {
        $repo = $this->doctrine->getRepository(Menu::class);
        $menu = $repo->findOneBy([
            'id' => $menuId
        ]);
        return $menu->getDish()->getPrice();
    }

    public function isOrderExist($order)
    {
        if ($order) {
            return $order;
        } else {
            return new Response('Error!', 400);
        }
    }
}