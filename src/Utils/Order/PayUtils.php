<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 27.11.18
 * Time: 23:59
 */

namespace App\Utils\Order;


use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class PayUtils extends AbstractDoctrineUtils
{

    private $security;

    public function __construct(EntityManager $doctrine, Security $security)
    {
        parent::__construct($doctrine);
        $this->security = $security;
    }

    public function payForOrder($order, $user)
    {
        $this->isOrderExist($order, $user);
        $this->isOrderReadyToPay($order);
        $order->setPaid(1);
        return $this->loadToBase($order);
    }

    private function isOrderExist($order, $user)
    {
        if (empty($order))
        {
            throw new Exception('Wrong order number', 400);
        } elseif ($order->getClient() !== $user) {
            throw new Exception('Not allowed', 406);
        }
    }

    private function isOrderReadyToPay($order)
    {
        $dishesInOrder = $this->findAllDishesIOrder($order);
        foreach ($dishesInOrder as $dish) {
            if ($dish->getReady() == null || $dish->getReady() == 0) {
                throw new Exception('Your order is not finished!' , 400);
            }
        }
    }

    private function findAllDishesIOrder($order)
    {
        $dishesInOrder = $this->doctrine->getRepository(OrderedMenu::class)->findBy([
            'ordered' => $order
        ]);
        return $dishesInOrder;
    }

    public function acceptPayment($order)
    {
        if (!$this->security->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException('Access denied');
        }
        $order->setFinished(1);
        $order->setEndDate(new \DateTime('now'));
        $this->loadToBase($order);
    }

    public function loadToBase($order)
    {
        $em = $this->doctrine;
        $em->persist($order);
        $em->flush();
        return true;
    }

}