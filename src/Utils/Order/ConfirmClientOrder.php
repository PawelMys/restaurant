<?php

namespace App\Utils\Order;

use App\Entity\ClientOrder;
use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class ConfirmClientOrder extends AbstractDoctrineUtils
{

    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(EntityManager $doctrine, RouterInterface $router)
    {
        parent::__construct($doctrine);
        $this->router = $router;
    }

    public function confirmOrder($user)
    {
        $clientOrder = $this->findClientOrder($user);
        $menuOrders = $this->getMenuOrders($clientOrder);
        return $this->loadToBase($menuOrders);
    }

    public function findClientOrder($user)
    {
        $repo = $this->doctrine->getRepository(ClientOrder::class);
        $order = $repo->findOneBy([
            'client' => $user
        ]);
        return $order;
    }

    public function getMenuOrders($order)
    {
        $repo = $this->doctrine->getRepository(OrderedMenu::class);
        $menuOrders = $repo->findBy([
            'ordered' => $order,
            'isConfirm' => 0
        ]);
        return $menuOrders;
    }

    public function loadToBase($menuOrders) {
        foreach ($menuOrders as $menuOrdered) {
            $this->baseManager($menuOrdered);
        }
        return new RedirectResponse($this->router->generate('client_orders'));
    }

    public function baseManager($order) {
            $em = $this->doctrine;
            $order->setIsConfirm(1);
            $em->persist($order);
            $em->flush();
    }
}