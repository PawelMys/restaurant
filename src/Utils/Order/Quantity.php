<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 19.12.18
 * Time: 10:09
 */

namespace App\Utils\Order;


use App\Entity\ClientOrder;
use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;

class Quantity extends AbstractDoctrineUtils
{

    private $totalPrice;

    public function __construct(EntityManager $doctrine, Price $totalPrice)
    {
        parent::__construct($doctrine);
        $this->totalPrice = $totalPrice;

    }

    public function changeQuantity($data)
    {
        $orderedMenu = $this->findMenuOrder($data->target);
        $clientOrder = $orderedMenu->getOrdered();
        $actualQty = $orderedMenu->getQuantity();
        $newQty = $this->countQuantity($actualQty, $data->type);
        $orderedMenu->setQuantity($newQty);
        $totalPrice = $this->totalPrice->countTotalChangeQty($orderedMenu, $clientOrder, $data->type);
        $clientOrder->setPrice($totalPrice);
        $em = $this->doctrine;
        $em->persist($clientOrder);
        $em->persist($orderedMenu);
        $em->flush();

        return [
            'totalPrice' => $totalPrice,
            'quantity' => $newQty
        ];
    }

    public function findMenuOrder($id)
    {
        $orderedMenu = $this->doctrine->getRepository(OrderedMenu::class)->findOneBy([
            'id' => $id
        ]);
        if ($orderedMenu !== null) {
            return $orderedMenu;
        } else {
            throw new Exception('Menu order not found', 400);
        }
    }


    public function countQuantity($actualQty, $type)
    {
        $this->isValueCorrect($actualQty, $type);
        if ($type == 'plus') {
            return $actualQty + 1;
        } else {
            return $actualQty + -1;
        }
    }

    public function isValueCorrect($actualQty, $type)
    {
        if ($actualQty > 1 && $type == 'minus') {
            return true;
        } elseif ($actualQty >= 1 && $type == 'plus') {
            return true;
        } else {
            throw new Exception('Value is incorrect', 400);
        }
    }

}