<?php

namespace App\Utils\Order;

use App\Entity\ClientOrder;
use App\Entity\Menu;
use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class CreateClientOrder extends AbstractDoctrineUtils
{

    private $totalPrice;

    public function __construct(EntityManager $doctrine, Price $totalPrice)
    {
        parent::__construct($doctrine);
        $this->totalPrice = $totalPrice;
    }

    public function saveOrder($data, $user = null)
    {
        if (!isset($data->orderId)) {
            $orderId = null;
        } else {
            $orderId = $data->orderId;
        }
        $em = $this->doctrine;
        $clientOrder = $this->isOrderExist($user, $orderId);
//        $clientOrder->setClient($user);
        $orderData = $this->saveOrderMenu($data->id, $clientOrder, $data->quantity, $data->isConfirm);
//        $total = $this->totalPrice($orderData['price'], $waiterOrder);
        $total = $this->totalPrice->totalPriceAfterAddDish($orderData, $clientOrder);
        $clientOrder->setPrice($total);
        try {
            $em->persist($clientOrder);
        } catch (ORMException $e) {
        }
        try {
            $em->flush();
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }
        return [
            'totalPrice' => $total,
            'orderId' => $orderData->getId()
        ];
    }


    public function saveOrderMenu($menuId, $clientOrder, $quantity = 1, $isConfirm = 0)
    {
//        $price = 0;
        $orderedMenu = new OrderedMenu();
        $menu = $this->doctrine->getRepository(Menu::class)->findOneBy([
            'id' => $menuId
        ]);
//        $price += $menu->getDish()->getPrice();
        $orderedMenu->setOrdered($clientOrder);
        $orderedMenu->setMenu($menu);
        $orderedMenu->setIsConfirm($isConfirm);
        $orderedMenu->setQuantity($quantity);
        try {
            $this->doctrine->persist($orderedMenu);
        } catch (ORMException $e) {

        }
        return $orderedMenu;
    }

    public function isOrderExist($user = null, $orderId = null )
    {
        if (isset($user) && !isset($orderId)) {
            $order = $this->doctrine->getRepository(ClientOrder::class)->findOneBy([
                'client' => $user
            ]);
        } elseif (isset($orderId) && !isset($user)) {
            $order = $this->doctrine->getRepository(ClientOrder::class)->findOneBy([
                'id' => $orderId
            ]);
        }
        if (!isset($order)) {
            $order = new ClientOrder();
            $order->setClient($user);
        };

        return $order;
    }
}