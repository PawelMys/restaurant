<?php


namespace App\Utils\Order;


use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;

class Price extends AbstractDoctrineUtils
{

    public function totalPriceAfterAddDish($newOrderedMenu, $order)
    {
        $orderedMenus = $this->doctrine->getRepository(OrderedMenu::class)->findBy([
            'ordered' => $order
        ]);
        $total = $newOrderedMenu->getQuantity()*$newOrderedMenu->getMenu()->getDish()->getPrice();
        foreach ($orderedMenus as $ordered) {
            $total += $ordered->getQuantity()*$ordered->getMenu()->getDish()->getPrice();
        }

        return $total;
    }

    public function countTotalChangeQty($orderedMenu, $order, $type)
    {
        $dishPrice = $orderedMenu->getMenu()->getDish()->getPrice();
        $oldTotalPrice = $order->getPrice();

        if ($type == 'minus') {
            $newTotalPrice = $oldTotalPrice - $dishPrice;
        } else {
            $newTotalPrice = $oldTotalPrice + $dishPrice;
        }

        return $newTotalPrice;
    }

    public function removeDish($clientOrder, $menuOrder)
    {
        $quantity = $menuOrder->getQuantity();
        $oldPrice = $clientOrder->getPrice();
        $dishPrice = $menuOrder->getMenu()->getDish()->getPrice();
        return $newPrice = $oldPrice - $quantity*$dishPrice;
    }

}