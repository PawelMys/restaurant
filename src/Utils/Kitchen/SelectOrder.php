<?php

namespace App\Utils\Kitchen;


use App\Entity\ClientOrder;
use App\Entity\Kitchen;
use App\Entity\OrderedMenu;
use App\Utils\AbstractDoctrineUtils;
use HttpRequestException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class SelectOrder extends AbstractDoctrineUtils
{
    public function setOrder($data, $user)
    {
        $this->isUserLoggedIn($user);
        $clientOrder = $this->getClientOrder($data);
        $orderedMenu = $this->getOrderedMenu($data);
        $this->findOrderInKitchen($clientOrder, $orderedMenu);
        return $this->loadToBase($orderedMenu, $clientOrder, $user);
    }

    public function isUserLoggedIn($user)
    {
        if ($user)
        {
            return true;
        } else {
            throw new Exception('Access denied. Wrong user!', 403);
        }
    }

    public function getClientOrder($data) {
        $repo = $this->doctrine->getRepository(ClientOrder::class);
        $clientOrder =  $repo->findOneBy([
            'id' => $data->orderId
        ]);

        return $this->ifRecordIsNotExist($clientOrder, 'The client order is not exist!');
    }

    public function getOrderedMenu($data)
    {
        $repo = $this->doctrine->getRepository(OrderedMenu::class);
        $orderedMenu =  $repo->findOneBy([
            'id' => $data->menuId
        ]);
        return $this->ifRecordIsNotExist($orderedMenu, 'Dish in menu is not exist!');
    }

    public function findOrderInKitchen($clientOrder, $menuOrder)
    {
        $repo = $this->doctrine->getRepository(Kitchen::class);
        $kitchen = $repo->findOneBy([
            'orderedDish' => $menuOrder,
            'clientOrder' => $clientOrder
        ]);
        return $this->isRecordExist($kitchen, 'This order is is already occupied!');
    }

    public function isRecordExist($record, $message)
    {
        if ($record) {
            throw new Exception($message, '409');
        } else {
            return $record;
        }
    }

    public function ifRecordIsNotExist($record, $message)
    {
        if (!$record) {
            throw new Exception($message, '400');
        } else {
            return $record;
        }
    }

    public function loadToBase($orderedMenu, $clientOrder, $user)
    {
        $kitchen = new Kitchen();
        $kitchen->setCook($user);
        $kitchen->setOrderedDish($orderedMenu);
        $kitchen->setClientOrder($clientOrder);
        $kitchen->setOrderedDish($orderedMenu);
        $em = $this->doctrine;
        $em->persist($kitchen);
        $em->flush();
        return new JsonResponse([
            'success' => 'done'
        ]);
    }
}