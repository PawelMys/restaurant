<?php

namespace App\Utils\Kitchen;


use App\Utils\AbstractDoctrineUtils;
use Symfony\Component\Config\Definition\Exception\Exception;

class FinishOrder extends AbstractDoctrineUtils
{
    public function finish($kitchenOrder, $user)
    {
        $this->isOrderExist($kitchenOrder);
        $this->checkUser($kitchenOrder, $user);
        $this->isOrderNotFinished($kitchenOrder);
        return  $this->loadToBase($kitchenOrder);
    }

    public function isOrderExist($kitchenOrder)
    {
        if (empty($kitchenOrder)) {
            throw new Exception('Order is not exist in your list', 400);
        }
    }
    public function checkUser($kitchen, $user)
    {
        if ($kitchen->getcook() == $user) {
            return true;
        } else {
            throw new Exception('Access denied', 403);
        }
    }

    public function isOrderNotFinished($kitchenOrder)
    {
        if ($kitchenOrder->getFinishTime() == null) {
            return true;
        } else {
            throw new Exception('You already finished this order', 400);
        }
    }

    public function loadToBase($kitchen)
    {
        $orderedDish = $kitchen->getOrderedDish();
        $orderedDish->setReady(1);
        $kitchen->setFinishTime(new \DateTime('now'));
        $em = $this->doctrine;
        $em->persist($orderedDish);
        $em->persist($kitchen);
        $em->flush();
        return true;
    }

}