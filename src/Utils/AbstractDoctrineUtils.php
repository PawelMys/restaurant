<?php

namespace App\Utils;


use Doctrine\ORM\EntityManager;

abstract class AbstractDoctrineUtils
{
    /**
     * @var EntityManager
     */
    protected $doctrine;

    /**
     * Pagination constructor.
     * @param EntityManager $doctrine
     */
    public function __construct(EntityManager $doctrine)
    {
        $this->doctrine = $doctrine;

    }
}