<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.08.18
 * Time: 09:28
 */

namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends Fixture
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * UserFixtures constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = [
        self::users('admin', '123', '2018-08-03 12:00:00', ['ROLE_SUPER_ADMIN'], 'Tomasz', 'Eloszka'),
        self::users('pawel', '123', '2018-08-04 11:00:00', ['ROLE_USER'], 'Pawel', 'Nowak'),
        self::users('test', 'wsad', '2018-08-04 12:32:21', ['ROLE_USER'], 'Grzegorz', 'Brzęczyszczykiewicz'),
        ];
        $encoder = $this->container->get('security.password_encoder');
        foreach ($users as $details) {
            $user = new User();
            $password = $encoder->encodePassword($user, $details['password']);
            $user->setUsername($details['username']);
            $user->setPassword($password);
            $user->setCreateDate(new \DateTime($details['createDate']));
            $user->setRoles($details['roles']);
            $user->setFirstName($details['firstName']);
            $user->setLastName($details['lastName']);
            $manager->persist($user);
        }
        $manager->flush();
    }

    public function users(string $username, string $password, string $createDate,array $roles, string $firstName, string $lastName)
    {
        return [
            'username' => $username,
            'password' => $password,
            'createDate' => $createDate,
            'roles' => $roles,
            'firstName' => $firstName,
            'lastName' => $lastName
        ];
    }

}