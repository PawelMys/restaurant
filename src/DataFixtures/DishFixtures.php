<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.08.18
 * Time: 09:28
 */

namespace App\DataFixtures;

use App\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DishFixtures extends Fixture
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $menuList = [
        self::menu('Scrambled eggs',
            'scrambled-eggs',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ex.',
            'BEAT eggs, milk, salt and pepper in medium bowl until blended. <p>HEAT butter in large nonstick skillet over medium heat until hot. POUR IN egg mixture. As eggs begin to set, GENTLY PULL the eggs across the pan with a spatula, forming large soft curds. <p>CONTINUE cooking – pulling, lifting and folding eggs – until thickened and no visible liquid egg remains. Do not stir constantly. REMOVE from heat. SERVE immediately.',
            19.99),
        self::menu('Hamburger',
            'hamburger',
            'Nam ac tellus leo. Aenean nunc velit, tincidunt sit amet sem at, finibus interdum mi.',
            'In a bowl, mix ground beef, egg, onion, bread crumbs, Worcestershire, garlic, 1/2 teaspoon salt, and 1/4 teaspoon pepper until well blended. Divide mixture into four equal portions and shape each into a patty about 4 inches wide.<p>Lay burgers on an oiled barbecue grill over a solid bed of hot coals or high heat on a gas grill (you can hold your hand at grill level only 2 to 3 seconds); close lid on gas grill. Cook burgers, turning once, until browned on both sides and no longer pink inside (cut to test), 7 to 8 minutes total. Remove from grill.<p>Lay buns, cut side down, on grill and cook until lightly toasted, 30 seconds to 1 minute.<p>Spread mayonnaise and ketchup on bun bottoms. Add lettuce, tomato, burger, onion, and salt and pepper to taste. Set bun tops in place.',
            10.50),
        self::menu('Casserole',
            'casserole',
            'Interdum et malesuada fames ac ante ipsum primis.',
            'Preheat oven to 350 degrees F (175 degrees C). <p> Heat oil in a large skillet over high heat. Saute onion, red bell pepper, and green bell pepper in hot oil until warmed through, about 2 minutes. <p> Combine onion-pepper mixture, cream of mushroom soup, cream of chicken soup, diced tomatoes, chicken broth, sour cream, cumin, ancho chile powder, oregano, and chipotle chile powder together in a large bowl and stir until sauce is well-combined. <p> Spread a few tablespoons of the sauce in the bottom of a 9x13-inch baking dish. Spread 1/2 the chicken over the sauce. Spread about half the sauce over the chicken and top with 1/3 the cheese. Spread a layer of tortillas over the cheese. Spread remaining 1/2 the chicken over the tortillas, and top with almost all of the remaining sauce, reserving 1/2 cup sauce. Top with 1/3 the cheese, remaining tortillas, the reserved 1/2 cup sauce, and remaining 1/3 cheese. <p> Bake casserole in the preheated oven until bubbling, about 40 minutes. Increase the oven temperature to broil. Broil the casserole until top is golden, 2 to 3 minutes more.',
            4),
        ];
        foreach ($menuList as $details) {
            $menu = new Dish();
            $menu->setName($details['name']);
            $menu->setSlug($details['slug']);
            $menu->setDescription($details['description']);
            $menu->setRecipe($details['recipe']);
            $menu->setPrice($details['price']);
            $menu->setImage('dish_image.jpg');
            $manager->persist($menu);
        }
        $manager->flush();
    }

    public function menu(string $name, string $slug, string $description, string $recipe, string $price)
    {
        return [
            'name' => $name,
            'slug' => $slug,
            'description' => $description,
            'recipe' => $recipe,
            'price' => $price
        ];
    }
}