<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 08.08.18
 * Time: 12:43
 */

namespace App\Controller;


use App\Entity\Category;
use App\Form\CategoryType;
use App\Utils\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("admin/category/new", name="admin_new_category", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $count = $this->getDoctrine()->getRepository(Category::class)->countCategory();
            $category->setPosition($count + 1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $this->addFlash('success', 'New MoveCategory added to the category');
            return $this->redirectToRoute('admin_category_list');
        }
        return $this->render('category/newCategory.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/category/edit/{slug}", name="admin_edit_category", methods={"GET", "POST"})
     * @ParamConverter("category", class="App\Entity\Category")
     * @param Category $category
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Category $category, Request $request)
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $this->addFlash('success', 'MoveCategory has been edited');
            return $this->redirectToRoute('admin_category_list');
        }
        return $this->render('category/editCategory.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/category/{page}", name="admin_category_list", defaults={"page" = 1}, requirements={"page" = "\d+"}, methods={"GET"})
     * @param $page
     * @param Pagination $pagination
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryList($page, Pagination $pagination)
    {
        $paginator = $pagination->paginateAll(Category::class, $page);
        return $this->render('category/categoryList.html.twig', [
            'paginator' => $paginator
        ]);
    }

    /**
     * @Route("admin/category/delete/{slug}", name="admin_delete_category", methods={"GET", "POST"})
     * @ParamConverter("category", class="App\Entity\Category")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();
        $this->addFlash('success', 'MoveCategory deleted');
        return $this->redirectToRoute('admin_category_list');
    }

    /**
     * @Route("admin/category/{slug}", name="admin_category", methods={"GET"})
     * @ParamConverter("category", class="App\Entity\Category")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminShow(Category $category)
    {
        return $this->render('category/showAdminCategory.html.twig', [
            'category' => $category
        ]);
    }
}