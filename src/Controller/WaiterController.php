<?php

namespace App\Controller;


use App\Entity\ClientOrder;
use App\Entity\Menu;
use App\Entity\OrderedMenu;
use App\Form\CreateOrderByWaiter;
use App\Utils\Archive\ArchiveOrders;
use App\Utils\Order\CreateClientOrder;
use App\Utils\Order\PayUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Config\Definition\Exception\Exception;

class WaiterController extends AbstractController
{

    /**
     * @Route("/waiter/index", name="waiter_index")
     */
    public function index()
    {

        return $this->render('waiter/index.html.twig');
    }

    /**
     * @Route("/waiter/orders", name="waiter_orders")
     */
    public function orders()
    {
        $orders = $this->getDoctrine()->getRepository(ClientOrder::class)->findAll();
        return $this->render('waiter/orders.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/waiter/order/{id}", name="waiter_show_order")
     * @ParamConverter("order", options={"mapping": {"id": "id"}})
     * @param ClientOrder $order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOrder(ClientOrder $order)
    {
        if ($order->getCallTheWaiter() == 1) {
            $order->setCallTheWaiter(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            $this->addFlash('success', 'This client is called you');
        }
        return $this->render('waiter/showOrders.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route("/waiter/payment/order/{id}", name="waiter_accept_payment")
     * @ParamConverter("order", options={"mapping": {"id": "id"}})
     * @param ClientOrder $order
     * @param PayUtils $payUtils
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function acceptPayment(ClientOrder $order, PayUtils $payUtils)
    {
        $payUtils->acceptPayment($order);
        return $this->redirectToRoute('waiter_orders');
    }

    /**
     * @Route("/waiter/order/remove-dish/{id}", name="waiter_remove_dish")
     * @ParamConverter("dish", options={"mapping": {"id": "id"}})
     * @param OrderedMenu $dish
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeDinerFromOrder(OrderedMenu $dish, Request $request)
    {
        $route = $request->headers->get('referer');
        if ($dish->getReady() == 1) {
            $this->addFlash('error', 'You can not remove. This dish is finished');
            return $this->redirect($route);
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dish);
            $em->flush();
            $this->addFlash('success', 'Dish removed');
            return $this->redirect($route);
        }
    }

    /**
     * @Route("/waiter/edit/order/{id}", name="waiter_edit_order")
     * @ParamConverter("order", options={"mapping": {"id": "id"}})
     * @param ClientOrder $order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editOrder(ClientOrder $order)
    {
//        $form = $this->createForm(CreateOrderByWaiter::class);
        $menu = $this->getDoctrine()->getRepository(Menu::class)->sortMenu();
        return $this->render('waiter/editOrder.html.twig', [
//            'form' => $form->createView(),
            'menu' => $menu,
            'order' => $order
        ]);
    }

    /**
     * @Route("/waiter/orders/add-dish", name="waiter_order_add_dish")
     * @param Request $request
     * @param CreateClientOrder $order
     */
    public function addDishToOrder(Request $request, CreateClientOrder $order)
    {
        $data = json_decode($request->getContent());
        try {
            if ($data !== null) {
                $res = $order->saveOrder($data);
                $this->addFlash('success', 'Order edited');
                return new JsonResponse(['type' => 'success']);
            } else {
                throw  new Exception('Data is not send', 400);
            }

        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('waiter_orders');
        }
    }

    /**
     * @Route("/waiter/create/order", name="waiter_create_order")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createOrder(Request $request)
    {
        $order = new ClientOrder();
        $form = $this->createForm(CreateOrderByWaiter::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order->setPrice(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            $this->addFlash('success', 'Order created');
            return $this->redirectToRoute('waiter_orders');
        }
        return $this->render('waiter/createOrder.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/waiter/archive-order/{id}", name="waiter_archive_order")
     * @ParamConverter("order", options={"mapping": {"id": "id"}})
     * @param ClientOrder $order
     * @param ArchiveOrders $archive
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function archiveOrder(ClientOrder $order, ArchiveOrders $archive)
    {
        $archive->archiveOrder($order, $this->getUser());
        $this->addFlash('success', 'Order has been moved to archive');
        return $this->redirectToRoute('waiter_orders');
    }

}