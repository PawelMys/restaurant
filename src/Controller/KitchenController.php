<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 21.11.18
 * Time: 11:14
 */

namespace App\Controller;


use App\Entity\ClientOrder;
use App\Entity\Kitchen;
use App\Utils\Kitchen\FinishOrder;
use App\Utils\Kitchen\SelectOrder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class KitchenController extends AbstractController
{

    /**
     * @Route("/kitchen", name="kitchen_order_list")
     */
    public function orderList()
    {
        $repo = $this->getDoctrine()->getRepository(ClientOrder::class);
        $orders = $repo->findAll();

        return $this->render('kitchen/orderList.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/kitchen/select/order", name="kitchen_select_order", methods={"POST"})
     * @param Request $request
     * @param SelectOrder $selectOrder
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Exception
     */
    public function selectOrder(Request $request, SelectOrder $selectOrder)
    {
        $jsonData = json_decode($request->getContent());
        try {
            if ($jsonData) {
                return $selectOrder->setOrder($jsonData, $this->getUser());
            } else {
                throw new Exception('No send data.', 400);
            }
        } catch (Exception $e)
        {
            return new JsonResponse([
                'errorMsg' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/kitchen/order/finished/{waiterOrder}/{orderedDish}", name="kitchen_order_finished")
     * @ParamConverter("kitchen", options={"mapping": {"waiterOrder": "waiterOrder", "orderedDish": "orderedDish"}})
     * @param Kitchen $kitchen
     * @param FinishOrder $finishOrder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function orderFinished(Kitchen $kitchen = null, FinishOrder $finishOrder)
    {
        try {
            $finished = $finishOrder->finish($kitchen, $this->getUser());
            if ($finished == true) {
                $this->addFlash('success', 'Done!');
                return $this->redirectToRoute('kitchen_order_list');
            } else {
                throw new Exception('Error', 400);
            }
        } catch (Exception $e)
        {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('kitchen_order_list');
        }
    }

    /**
     * @Route("kitchen/your_orders", name="kitchen_your_orders")
     */
    public function yourOrders()
    {
        $kitchenOrders = $this->getDoctrine()->getRepository(Kitchen::class)->findBy([
            'cook' => $this->getUser()
        ]);
        return $this->render('kitchen/yourOrders.twig', [
            'orders' => $kitchenOrders
        ]);
    }
}