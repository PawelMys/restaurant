<?php

namespace App\Controller;

use App\Entity\Dish;
use App\Form\DishType;
use App\Service\FileUpload;
use App\Utils\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DishController extends AbstractController
{
    /**
     * @Route("admin/dishes/new", name="admin_new_dish", methods={"GET", "POST"})
     * @param Request $request
     * @param FileUpload $fileUpload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request, FileUpload $fileUpload)
    {
        $dish = new Dish();
        $form = $this->createForm(DishType::class, $dish);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

//            If gets error check permissions in public/img folder

            $file = $dish->getImage();
            $fileName = $fileUpload->upload($file);
            $dish->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();
            $this->addFlash('success', 'New dish added to the category');
            return $this->redirectToRoute('admin_index');
        }
        return $this->render('dish/newDish.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/dishes/{page}", name="admin_dishes", defaults={"page" = 1}, requirements={"page" = "\d+"}, methods={"GET"})
     * @param $page
     * @param Pagination $pagination
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function AdminDishes($page, Pagination $pagination)
    {
        $paginator = $pagination->paginateAll(Dish::class, $page);
        return $this->render('dish/dishes.html.twig', [
            'paginator' => $paginator
        ]);
    }

    /**
     * @Route("admin/dishes/edit/{slug}", name="admin_edit_dish", methods={"GET", "POST"})
     * @ParamConverter("dish", class="App\Entity\Dish")
     * @param Dish $dish
     * @param Request $request
     * @param FileUpload $fileUpload
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Dish $dish, Request $request, FileUpload $fileUpload)
    {
        $actualImg = $dish->getImage();
        $form = $this->createForm(DishType::class, $dish);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //            If gets error check permissions in public/img folder

            $newImg = $dish->getImage();
            $fileName = $fileUpload->edit($newImg, $actualImg);
            $dish->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();
            $this->addFlash('success', 'Dish has been edited');
            return $this->redirectToRoute('admin_dishes');
        }
        return $this->render('dish/editDish.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("admin/dishes/delete/{slug}", name="admin_delete_dish", methods={"GET", "POST"})
     * @ParamConverter("dish", class="App\Entity\Dish")
     * @param Dish $dish
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Dish $dish)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($dish);
        $em->flush();
        $this->addFlash('success', 'Dish deleted');
        return $this->redirectToRoute('admin_dishes');
    }

    /**
     * @Route("admin/dish/{slug}", name="admin_dish", methods={"GET"})
     * @ParamConverter("dish", class="App\Entity\Dish")
     * @param Dish $dish
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminShow(Dish $dish)
    {
        return $this->render('dish/showAdminDish.html.twig', [
            'dish' => $dish
        ]);
    }


}