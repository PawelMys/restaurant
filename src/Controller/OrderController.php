<?php

namespace App\Controller;


use App\Entity\ClientOrder;
use App\Entity\Menu;
use App\Utils\Order\ConfirmClientOrder;
use App\Utils\Order\CreateClientOrder;
use App\Utils\Order\PayUtils;
use App\Utils\Order\Price;
use App\Utils\Order\Quantity;
use App\Utils\Order\RemoveClientOrder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    /**
     * @Route("/client/menu", name="client_menu")
     */
    public function menu()
    {
        $menuRepo = $this->getDoctrine()->getRepository(Menu::class);
        $dishInMenu = $menuRepo->sortMenu();
        $orderRepo = $this->getDoctrine()->getRepository(ClientOrder::class);
        $clientOrder = $orderRepo->findOneBy([
            'client' => $this->getUser()
        ]);
        return $this->render('order/clientMenu.html.twig', [
            'menu' => $dishInMenu,
            'order' => $clientOrder
        ]);
    }

    /**
     * @Route("/client/save/new-order", name="client_save_new_order", methods={"POST"})
     * @param Request $request
     * @param CreateClientOrder $createClientOrder
     * @return Response|JsonResponse
     */
    public function saveOrder(Request $request, CreateClientOrder $createClientOrder)
    {
        if ($request->getContent()) {
            $order = json_decode($request->getContent());
            $orderData = $createClientOrder->saveOrder($order, $this->getUser());
            return new JsonResponse([
                'totalPrice' => $orderData['totalPrice'],
                'id' => $orderData['orderId']
            ]);
        }
        return new Response('Error!', 400);
    }

    /**
     * @Route("/client/order/all", name="client_all_orders")
     */
    public function existClientOrders()
    {
        $repo = $this->getDoctrine()->getRepository(ClientOrder::class);
        $orders = $repo->findOneBy([
            'client' => $this->getUser()
        ]);
        return $this->render('order/existClientOrders.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/client/order/remove", name="client_order_remove", methods={"POST"})
     * @param Request $request
     * @param RemoveClientOrder $removeClientOrder
     * @return Response
     */
    public function removeClientOrder(Request $request, RemoveClientOrder $removeClientOrder)
    {
        if ($request->getContent()) {
            $data = json_decode($request->getContent());
            $user = $this->getUser();
            return $removeClientOrder->removeItem($data, $user);

        } else {

            return new Response('error', 400);
        }
    }

    /**
     * @Route("/client/confirm/orders", name="client_confirm_order")
     * @param ConfirmClientOrder $confirmClientOrder
     * @return Response
     */
    public function confirmOrder(ConfirmClientOrder $confirmClientOrder)
    {
        $user = $this->getUser();
        return $confirmClientOrder->confirmOrder($user);
    }

    /**
     * @Route("/client/orders", name="client_orders")
     */
    public function orders()
    {
        $repo = $this->getDoctrine()->getRepository(ClientOrder::class);
        $order = $repo->findOneBy([
            'client' => $this->getUser()
        ]);
        return $this->render('order/orders.html.twig', [
            'orders' => $order
        ]);
    }

    /**
     * @Route("/client/pay/{orderId}", name="client_pay")
     * @ParamConverter("order", options={"mapping": {"orderId": "id"}})
     * @param ClientOrder $order
     * @param PayUtils $payUtils
     * @return Response
     */
    public function pay(ClientOrder $order = null, PayUtils $payUtils)
    {
        try {
            $payUtils->payForOrder($order, $this->getUser());
            return $this->render('order/pay.html.twig', [
                'order' => $order
            ]);
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
            return $this->redirectToRoute('client_orders');
        }
    }

    /**
     * @Route("/client/order/call-waiter/{orderId}", name="client_call_waiter")
     * @ParamConverter("order", options={"mapping": {"orderId": "id"}})
     * @param ClientOrder $order
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function callTheWaiter(ClientOrder $order, Request $request)
    {
        $route = $request->headers->get('referer');
        if ($order->getCallTheWaiter() == 0) {
            $order->setCallTheWaiter(1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            $this->addFlash('success', 'You call a waiter');
        } else {
            $this->addFlash('success', 'You already call a waiter');
        }
        return $this->redirect($route);
    }

    /**
     * @Route("/client/order/qty", name="client_order_qty")
     * @param Request $request
     * @param Quantity $quantity
     * @return JsonResponse
     */
    public function orderQuantity(Request $request, Quantity $quantity)
    {
        $data = json_decode($request->getContent());
        $json = $quantity->changeQuantity($data);

        return new JsonResponse($json);
    }
}