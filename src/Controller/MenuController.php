<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\AddDishToMenuType;
use App\Utils\Menu\CategoryPosition;
use App\Utils\Menu\MenuPosition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    /**
     * @Route("/admin/menu/add-dish", name="admin_add_dish_to_menu", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addDish(Request $request)
    {
        $menu = new Menu();
        $form = $this->createForm(AddDishToMenuType::class, $menu);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form['category']->getData();
            $repo = $this->getDoctrine()->getRepository(Menu::class);
            $count = $repo->countDish([
                'category' => $category
            ]);
            $menu->setDishPosition($count+1);
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush();
            $this->addFlash('success', 'Dish added to menu');
            return $this->redirectToRoute('admin_menu');
        }
        return $this->render('menu/addDish.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/menu", name="admin_menu", methods={"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dishesInMenu()
    {
        $repo = $this->getDoctrine()->getRepository(Menu::class);
        $menu = $repo->sortMenu();
        return $this->render('menu/menu.html.twig', [
            'menu' => $menu
        ]);
    }

    /**
     * @Route("admin/menu/delete-dish/{id}", name="admin_delete_dish_from_menu", methods={"GET", "POST"})
     * @ParamConverter("menu", class="App\Entity\Menu")
     * @param Menu $menu
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Menu $menu)
    {
        $em = $this->getDoctrine()->getManager();
        $position = $menu->getDishPosition();
        $changePosition = $this->getDoctrine()->getRepository(Menu::class)->findToChangePosition([
            'deletedPosition' => $position,
            'category' => $menu->getCategory()
        ]);
        foreach ($changePosition as $newPosition) {
            $oldPosition = $newPosition->getDishPosition();
            $newPosition->setDishPosition($oldPosition-1);
            $em->persist($newPosition);
        }
        $em->remove($menu);
        $em->flush();
        $this->addFlash('success', 'Dish removed form menu');
        return $this->redirectToRoute('admin_menu');
    }

    /**
     * @Route("admin/menu/edit-dish/{id}", name="admin_edit_dish_in_menu", methods={"GET", "POST"})
     * @ParamConverter("menu", class="App\Entity\Menu")
     * @param Menu $menu
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Menu $menu, Request $request)
    {
        $form = $this->createForm(AddDishToMenuType::class, $menu);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($menu);
            $em->flush();
            $this->addFlash('success', 'Dish has been edited');
            return $this->redirectToRoute('admin_menu');
        }
        return $this->render('dish/editDish.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/menu/dish/position/save", name="admin_menu_dish_position_save", methods={"POST"})
     * @param Request $request
     * @param MenuPosition $position
     * @return JsonResponse|Response
     */
    public function saveDishPosition(Request $request, MenuPosition $position)
    {
        if ($request->getContent()) {
            $move = json_decode($request->getContent());
            $movedDish = $position->findData($move);
            return $position->moveControl($move, $movedDish);
        }

        return new Response('Error!', 400);
    }

    /**
     * @Route("/admin/menu/category/position/save", name="admin_menu_category_position_save", methods={"POST"})
     * @param Request $request
     * @param CategoryPosition $position
     * @return Response
     */
    public function saveCategoryPosition(Request $request, CategoryPosition $position) {
        if ($request->getContent()) {
            $move = json_decode($request->getContent());
            $movedCategory = $position->findData($move);
            return $position->moveControl($move, $movedCategory);
        }
        return new Response('Error!', 400);
    }

}