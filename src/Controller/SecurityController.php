<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 02.08.18
 * Time: 15:27
 */

namespace App\Controller;


use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\EditUserType;
use App\Form\RegisterType;
use App\Utils\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("admin/users/new", name="admin_register_user", methods={"GET", "POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Account created.');
            return $this->redirectToRoute('admin_index');
        }
        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login", methods={"GET", "POST"})
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', [
           'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/admin/users/edit/{username}", name="admin_edit_user", methods={"GET", "POST"})
     * @ParamConverter("user", class="App\Entity\User")
     * @param User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAccount(User $user, Request $request)
    {
        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'User has been edited');
            return $this->redirectToRoute('admin_users');
        }
        return $this->render('security/editUser.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/users/{$page}", name="admin_users", defaults={"page" = 1}, requirements={"page" = "\d+"}, methods={"GET"})
     * @param $page
     * @param Pagination $pagination
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function users($page, Pagination $pagination)
    {
        $paginator = $pagination->paginateAll(User::class, $page);
        return $this->render('security/users.html.twig', [
            'paginator' => $paginator
        ]);
    }

    /**
     * @Route("/admin/users/delete/{username}", name="admin_delete_user", methods={"GET", "POST"})
     * @ParamConverter("user", class="App\Entity\User")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $this->addFlash('success', 'User has been deleted');
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/admin/users/change-password/{username}", name="admin_change_user_password", methods={"GET", "POST"})
     * @ParamConverter("user", class="App\Entity\User")
     * @param Request $request
     * @param User $user
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            $password = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'User password has been changed');
            return $this->redirectToRoute('admin_users');
        }
        return $this->render('security/changeUserPassword.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login/redirect", name="login_redirect")
     */
    public function loginRedirect()
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_index');
        } elseif ($this->isGranted('ROLE_CLIENT')) {
            return $this->redirectToRoute('client_menu');
        }
    }
}