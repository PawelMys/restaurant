<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.08.18
 * Time: 15:06
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class MenuRepository extends EntityRepository
{
    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('menu');
    }

    public function countDish(array $params = array())
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('COUNT(m.id)');
        $qb->leftJoin('m.category', 'c');
        $qb->where('c.id = :category')
            ->setParameter('category', $params['category']);

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function sortMenu()
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('m, c');
        $qb->leftJoin('m.category', 'c');
        $qb->orderBy('c.position', 'ASC')
            ->addOrderBy('m.dishPosition', 'ASC');


        return $qb->getQuery()->getResult();
    }
    public function findToChangePosition(array $params = array())
    {
        $qb = $this->createQueryBuilder('m');
        $qb->select('m, c');
        $qb->leftJoin('m.category', 'c');
        $qb->andwhere('m.dishPosition > :deletePosition AND c.id = :category')
            ->setParameters([
                'deletePosition' => $params['deletedPosition'],
                'category'=> $params['category']
            ]);
//            ->setParameter(':deletePosition', $params['deletedPosition']);
        return $qb->getQuery()->getResult();
    }
}