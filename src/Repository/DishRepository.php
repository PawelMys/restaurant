<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.08.18
 * Time: 15:06
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;

class DishRepository extends EntityRepository
{
    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('dish');
    }
}