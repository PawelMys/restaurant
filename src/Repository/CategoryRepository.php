<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.08.18
 * Time: 15:06
 */

namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;

class CategoryRepository extends EntityRepository
{
    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('category');
    }

    public function countCategory()
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select('COUNT(c.id)');

        try {
            return $qb->getQuery()->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}