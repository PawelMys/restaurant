<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category extends AbstractMenu
{
    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Menu",
     *     mappedBy="category"
     * )
     */
    private $menu;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position;

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

}