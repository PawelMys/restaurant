<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Kitchen
 * @ORM\Entity(repositoryClass="App\Repository\KitchenRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Kitchen
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="cook_id", referencedColumnName="id")
     */
    private $cook;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\OrderedMenu", inversedBy="kitchen")
     * @ORM\JoinColumn(name="ordered_dish", referencedColumnName="id", nullable=true)
     */
    private $orderedDish;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientOrder")
     * @ORM\JoinColumn(name="client_order", referencedColumnName="id")
     */
    private $clientOrder;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishTime;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCook()
    {
        return $this->cook;
    }

    /**
     * @param mixed $cook
     */
    public function setCook($cook): void
    {
        $this->cook = $cook;
    }

    /**
     * @return mixed
     */
    public function getOrderedDish()
    {
        return $this->orderedDish;
    }

    /**
     * @param mixed $orderedDish
     */
    public function setOrderedDish($orderedDish): void
    {
        $this->orderedDish = $orderedDish;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getFinishTime()
    {
        return $this->finishTime;
    }

    /**
     * @param mixed $finishTime
     */
    public function setFinishTime($finishTime): void
    {
        $this->finishTime = $finishTime;
    }

    /**
     * @return mixed
     */
    public function getClientOrder()
    {
        return $this->clientOrder;
    }

    /**
     * @param mixed $clientOrder
     */
    public function setClientOrder($clientOrder): void
    {
        $this->clientOrder = $clientOrder;
    }

    /**
     * @ORM\PrePersist()
     */
    public function generateDate()
    {
        $this->startTime = new \DateTime('now');
    }
}