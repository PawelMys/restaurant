<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 06.08.18
 * Time: 12:01
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DishRepository")
 */
class Dish extends AbstractMenu
{
    /**
     * @ORM\Column(type="string", length=2056)
     * @Assert\NotBlank()
     */
    private $recipe;

    /**
     * @ORM\Column(type="string")
     * @Assert\Type("numeric")
     */
    private $price;
    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Menu",
     *     mappedBy="dish"
     * )
     */
    private $menu;


    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Plese load image")
     * @Assert\Image(
     *     minHeight="560",
     *     maxHeight="560",
     *     maxWidth="400",
     *     minWidth="400"
     * )
     */
    private $image;

    /**
     * @return mixed
     */
    public function getRecipe()
    {
        return $this->recipe;
    }

    /**
     * @param mixed $recipe
     */
    public function setRecipe($recipe): void
    {
        $this->recipe = $recipe;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = str_replace(',', '.', $price);
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }



}