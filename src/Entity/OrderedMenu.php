<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class OrderedMenu
 * @ORM\Entity(repositoryClass="App\Repository\OrderedMenuRepository")
 * @ORM\Table(name="ordered_menu")
 */
class OrderedMenu
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientOrder", inversedBy="ordered")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $ordered;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Menu", inversedBy="ordered")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     */
    private $menu;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $isConfirm;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ready;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Kitchen", mappedBy="orderedDish", cascade={"remove"})
     */
    private $kitchen;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

    /**
     * @param mixed $ordered
     */
    public function setOrdered($ordered): void
    {
        $this->ordered = $ordered;
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param mixed $menu
     */
    public function setMenu($menu): void
    {
        $this->menu = $menu;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * @param mixed $ready
     */
    public function setReady($ready): void
    {
        $this->ready = $ready;
    }

    /**
     * @return mixed
     */
    public function getIsConfirm()
    {
        return $this->isConfirm;
    }

    /**
     * @param mixed $isConfirm
     */
    public function setIsConfirm($isConfirm): void
    {
        $this->isConfirm = $isConfirm;
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * @param mixed $kitchen
     */
    public function setKitchen($kitchen): void
    {
        $this->kitchen = $kitchen;
    }



}