<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class menu
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MenuRepository")
 */
class Menu
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Category",
     *     inversedBy="menu"
     * )
     */
    private $category;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Dish",
     *     inversedBy="menu"
     * )
     */
    private $dish;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dishPosition;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderedMenu", mappedBy="menu")
     */
    private $ordered;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getDish()
    {
        return $this->dish;
    }

    /**
     * @param mixed $dish
     */
    public function setDish($dish): void
    {
        $this->dish = $dish;
    }

    /**
     * @return mixed
     */
    public function getDishPosition()
    {
        return $this->dishPosition;
    }

    /**
     * @param mixed $dishPosition
     */
    public function setDishPosition($dishPosition): void
    {
        $this->dishPosition = $dishPosition;
    }

//    /**
//     * @return mixed
//     */
//    public function getOrder(): ?order
//    {
//        return $this->order;
//    }
//
//    /**
//     * @param mixed $order
//     * @return Menu
//     */
//    public function setOrder(?order $order): self
//    {
//        $this->order = $order;
//    }


}