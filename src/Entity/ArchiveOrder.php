<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArchiveOrderRepository")
 * @ORM\Table(name="archive_order")
 */
class ArchiveOrder
{

    /**
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @ORM\Id()
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=128)
     */
    private $orderId;

    /**
     * @ORM\Column(type="array")
     */
    private $orderedDish;

    /**
     * @ORM\Column(type="datetime")
     */
    private $orderedTime;

    /**
     * @ORM\Column(type="datetime")
     */
    private $paidTime;

    /**
     * @ORM\Column(type="string")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="id")
     */
    private $waiter;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getOrderedDish()
    {
        return $this->orderedDish;
    }


    public function setOrderedDish(array $orderedDish): void
    {
        $this->orderedDish = $orderedDish;
//        $arr = [];
//        foreach ($orderedDish as $order) {
//            $data = [
//                'menu' => $order['menu'],
//                'dish' => $order['dish'],
//                'kitchen' => $order['kitchen'],
//            ];
//            array_push($arr, $data);
//        }
//        $this->orderedDish = $arr;
    }

    /**
     * @return mixed
     */
    public function getOrderedTime()
    {
        return $this->orderedTime;
    }

    /**
     * @param mixed $orderedTime
     */
    public function setOrderedTime($orderedTime): void
    {
        $this->orderedTime = $orderedTime;
    }

    /**
     * @return mixed
     */
    public function getPaidTime()
    {
        return $this->paidTime;
    }

    /**
     * @param mixed $paidTime
     */
    public function setPaidTime($paidTime): void
    {
        $this->paidTime = $paidTime;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getWaiter()
    {
        return $this->waiter;
    }

    /**
     * @param mixed $waiter
     */
    public function setWaiter($waiter): void
    {
        $this->waiter = $waiter;
    }




}