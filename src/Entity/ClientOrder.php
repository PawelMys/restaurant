<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ClientOrder
 * @ORM\Entity(repositoryClass="App\Repository\ClientOrderRepository")
 * @ORM\Table(name="client_order")
 * @ORM\HasLifecycleCallbacks()
 */
class ClientOrder
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderedMenu", mappedBy="ordered", cascade={"remove"})
     */
    private $ordered;

    /**
     * @ORM\Column(type="string")
     * @Assert\Type("numeric")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $paid = 0;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $finished = 0;

    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private $callTheWaiter = 0;

    function __construct()
    {
        $this->ordered = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrdered()
    {
        return $this->ordered;
    }

//    public function addOrdered(OrderedMenu $orderededMenu)
//    {
//        $this->ordered->add($orderedMenu);
//    }

    /**
     * @param mixed $ordered
     */
    public function setOrdered($ordered): void
    {
        $this->ordered = $ordered;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @ORM\PrePersist()
     */
    public function generateDate()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @param mixed $paid
     */
    public function setPaid($paid): void
    {
        $this->paid = $paid;
    }

    /**
     * @return mixed
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * @param mixed $finished
     */
    public function setFinished($finished): void
    {
        $this->finished = $finished;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client): void
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getCallTheWaiter()
    {
        return $this->callTheWaiter;
    }

    /**
     * @param mixed $callTheWaiter
     */
    public function setCallTheWaiter($callTheWaiter): void
    {
        $this->callTheWaiter = $callTheWaiter;
    }


}