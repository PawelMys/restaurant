<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 13.08.18
 * Time: 11:53
 */

namespace App\Form;


use App\Entity\Menu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddDishToMenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'label' => 'Choice category',
                'class' => 'App\Entity\Category',
                'choice_label' => 'name'
            ])
            ->add('dish', EntityType::class, [
                'label' => 'Choice dish',
                'class' => 'App\Entity\Dish',
                'choice_label' => 'name'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Submit'
            ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class
        ]);
    }
}