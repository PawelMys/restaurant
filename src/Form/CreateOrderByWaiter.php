<?php
/**
 * Created by PhpStorm.
 * User: pawel
 * Date: 17.12.18
 * Time: 14:48
 */

namespace App\Form;


use App\Entity\ClientOrder;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateOrderByWaiter extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client', EntityType::class,[
                'label' => 'Choice client',
                'multiple' => false,
                'class' => User::class,
                'choice_label' => 'username'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Create order'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ClientOrder::class,
        ]);
    }
}