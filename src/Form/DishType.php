<?php

namespace App\Form;


use App\Entity\Dish;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'empty_data' => 'Name of dish'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Short description',
                'empty_data' => 'Short description'
            ])
            ->add('Recipe', TextareaType::class, [
                'label' => 'Recipe',
                'empty_data' => 'Recipe'
            ])
            ->add('price', TextType::class, [
                'label' => 'Price'
                ])
            ->add('image', FileType::class, [
                'label' => 'Image 280x200px',
                'data_class' => null,
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Submit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dish::class,
        ]);
    }

}