<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
class FileUpload
{

    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file)
    {
        $filename = md5(uniqid()).'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $filename);
        } catch (FileException $e) {
            throw new FileException($e->getMessage());
        }
        return $filename;
    }

    public function edit($newImg, $oldImg)
    {
        if ($newImg == null) {
            return $oldImg;
        } else {
            return $this->upload($newImg);
        }
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

}